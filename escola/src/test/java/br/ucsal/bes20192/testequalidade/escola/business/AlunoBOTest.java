package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

class AlunoBOTest {

	@Test
	void testarAtualizacaoAlunoAtivo() {

		AlunoDAO alunoDAOMock = Mockito.mock(AlunoDAO.class);
		
		AlunoBO alunoBO = new AlunoBO(alunoDAOMock, null);

		Aluno aluno = AlunoBuilder.umAlunoAtivo().build();

		alunoBO.atualizar(aluno);
		
		Mockito.verify(alunoDAOMock).salvar(aluno);
	}

}
