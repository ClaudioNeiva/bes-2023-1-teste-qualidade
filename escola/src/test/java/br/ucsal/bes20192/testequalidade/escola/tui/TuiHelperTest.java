package br.ucsal.bes20192.testequalidade.escola.tui;

import static org.mockito.Mockito.calls;

import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class TuiHelperTest {
	
	private Scanner scannerMock = Mockito.mock(Scanner.class);
	
	@Test
	void testarObterNomeCompleto() {
		Mockito.when(scannerMock.nextLine()).thenReturn("claudio").thenReturn("neiva");
		
		String nomeCompletoEsperado = "claudio neiva";
		
		TuiHelper tuiHelper = new TuiHelper();
		tuiHelper.scanner = scannerMock;
		
		String nomeCompletoAtual = tuiHelper.obterNomeCompleto();
		
		Assertions.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

}
