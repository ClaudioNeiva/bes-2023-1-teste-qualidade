package br.ucsal.bes20192.testequalidade.escola.domain;

public enum SituacaoAluno {
	ATIVO, CANCELADO;
}
