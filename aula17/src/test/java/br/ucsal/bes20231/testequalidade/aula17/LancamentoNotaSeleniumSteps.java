package br.ucsal.bes20231.testequalidade.aula17;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LancamentoNotaSeleniumSteps {

	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		// Comandos selenium!
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		// Comandos selenium!
	}

	@Then("a situação do aluno é $situacaoEsperada, pois a média de aprovação é $mediaAprovacaoEsperada")
	public void verificarSituacaoAluno(String situacaoEsperada, Double mediaAprovacaoEsperada) {
		// Comandos selenium!
	}

	@Then("a média do aluno é $mediaEsperada")
	public void verificarMediaAluno(Double mediaEsperada) {
		// Comandos selenium!
	}
}
