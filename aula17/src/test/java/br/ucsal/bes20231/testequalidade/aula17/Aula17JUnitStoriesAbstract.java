package br.ucsal.bes20231.testequalidade.aula17;

import java.util.Locale;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.MarkUnmatchedStepsAsPending;
import org.jbehave.core.steps.StepFinder;


public abstract class Aula17JUnitStoriesAbstract extends JUnitStories {
	
	@Override
	public Configuration configuration() {
		Keywords keywords = new LocalizedKeywords(Locale.of("pt"));
		return new MostUsefulConfiguration()
				.useKeywords(keywords)
				.useStepCollector(new MarkUnmatchedStepsAsPending(new StepFinder(), keywords))
				.useStoryReporterBuilder(new StoryReporterBuilder()
					.withDefaultFormats().withFormats(
							Format.CONSOLE, Format.TXT, Format.HTML));
	}

}
