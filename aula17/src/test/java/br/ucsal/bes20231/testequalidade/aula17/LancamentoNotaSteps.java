package br.ucsal.bes20231.testequalidade.aula17;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LancamentoNotaSteps {

	private Aluno aluno;

	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		// System.out.println("executando o passo: Dado que um aluno está matriculado na
		// disciplina");
		aluno = new Aluno();
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		// System.out.println("executando o passo: informo a nota, com parâmetro " +
		// nota);
		aluno.informarNota(nota);
	}

	@Then("a situação do aluno é $situacaoEsperada, pois a média de aprovação é $mediaAprovacaoEsperada")
	public void verificarSituacaoAluno(String situacaoEsperada, Double mediaAprovacaoEsperada) {
		// System.out.println("executando o passo: a situação do aluno é " +
		// situacaoEsperada);
		Assert.assertEquals(situacaoEsperada, aluno.obterSituacao());
		Assert.assertEquals(mediaAprovacaoEsperada, Aluno.MEDIA_APROVACAO);
	}

	@Then("a média do aluno é $mediaEsperada")
	public void verificarMediaAluno(Double mediaEsperada) {
		Assert.assertEquals(mediaEsperada, aluno.calcularMedia());
	}
}
