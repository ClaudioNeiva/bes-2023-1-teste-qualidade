package br.ucsal.bes20231.testequalidade.aula17;

import java.util.ArrayList;
import java.util.List;

public class Aluno {
	
	public static final Double MEDIA_APROVACAO = 7.;
	
	private List<Double> notas;

	public Aluno() {
		notas = new ArrayList<>();
	}

	public void informarNota(Double nota) {
		notas.add(nota);
	}

	public Double calcularMedia() {
		Double soma = 0d;
		for (Double nota : notas) {
			soma += nota;
		}
		return soma / notas.size();
	}

	public String obterSituacao() {
		if (calcularMedia() >= MEDIA_APROVACAO) {
			return "Aprovado";
		}
		return "Reprovado";
	}
}
