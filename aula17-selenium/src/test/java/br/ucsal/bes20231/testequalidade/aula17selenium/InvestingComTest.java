package br.ucsal.bes20231.testequalidade.aula17selenium;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

@TestInstance(Lifecycle.PER_CLASS)
class InvestingComTest {

	private static final long TIMEOUT_WAIT_WEB_DRIVER_SEGUNDOS = 5;
	private WebDriver driver;
	private WebDriverWait wait;

	@BeforeAll
	void setup() {
		// System.setProperty("webdriver.chrome.driver", "path??????/chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, TIMEOUT_WAIT_WEB_DRIVER_SEGUNDOS);
	}

	@AfterAll
	void teardown() {
		driver.quit();
	}

	@Test
	void testarPesquisa() throws InterruptedException {
		// Abrir página do Investing.com
		driver.get("http://br.investing.com");

		// Páginas html dentro do projeto
		// driver.get(String.valueOf(ClasseDoProjeto.class.getResource("/folder/pagina.html")));

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		// Obter o conteúdo da página
		// Thread.sleep(5000); // WebDriverWait?
		wait.until(ExpectedConditions.numberOfElementsToBe(By.className("js-inner-all-results-quotes-wrapper"), 1));

		String conteudo = driver.getPageSource();

		// Verificar se retorno inclui "Cogna Educação"
		Assertions.assertTrue(conteudo.contains("Cogna Educação"));

	}

}
