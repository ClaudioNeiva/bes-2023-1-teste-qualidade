package br.ucsal.bes20231.testequalidade.aula17selenium;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

@TestInstance(Lifecycle.PER_CLASS)
public class ImpostoTest {

	private WebDriver driver;

	@BeforeAll
	void setup() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
	}

	@AfterAll
	void teardown() {
		driver.quit();
	}

	/**
	 *	@throws InterruptedException 
	 * @formatter:off
	 	Cenário: Cálculo do imposto de renda com uso de deduções
		Dado que estou na funcionalidade de cálculo de imposto de renda
		Quando informo como rendimentos tributáveis 18895.65 reais
		E habilito a opção de informar deduções 
		E informo como previdência oficial 828.38 reais
		E informo 2 dependents
		E solicito que o cálculo seja realizado 
		Então o valor do imposto a pagar é de 3994.86 reais
		E a alíquota efetiva é de 21.14%
	 * 	@formatter:on
	 */
	@Test
	void testarImposto1() throws InterruptedException {
		// Dado que estou na funcionalidade de cálculo de imposto de renda
		driver.get(String.valueOf(ImpostoTest.class.getResource("/webapp/imposto.html")));

		// Quando informo como rendimentos tributáveis 18895.65 reais
		informarValorCampo("rendimentosTributaveis", "18895.65");

		// E habilito a opção de informar deduções
		clicarCampo("isInformarDeducoes");

		// E informo como previdência oficial 828.38 reais
		informarValorCampo("previdenciaOficial", "828.38");

		// E informo 2 dependents
		informarValorCampo("quantidadeDependentes", "2");

		// E solicito que o cálculo seja realizado
		clicarCampo("calculoBtn");

		// Então o valor do imposto a pagar é de 3994.86 reais
		// E a alíquota efetiva é de 21.14%
		Assertions.assertAll(
			() -> verificarValorCampo("impostoPagar", "3994.86"),
			() -> verificarValorCampo("aliquotaEfetiva", "21.14%"));
	}

	private void verificarValorCampo(String idCampo, String valorCampoEsperado) {
		WebElement campo = driver.findElement(By.id(idCampo));
		String valorCampoAtual = campo.getAttribute("value");
		Assertions.assertEquals(valorCampoEsperado, valorCampoAtual);
	}

	private void informarValorCampo(String idCampo, String valorCampo) {
		WebElement campo = driver.findElement(By.id(idCampo));
		campo.clear();
		campo.sendKeys(valorCampo);
	}

	private void clicarCampo(String idCampo) {
		driver.findElement(By.id(idCampo)).click();
	}
}
