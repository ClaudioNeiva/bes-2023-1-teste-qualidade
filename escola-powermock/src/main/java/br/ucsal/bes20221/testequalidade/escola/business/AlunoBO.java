package br.ucsal.bes20221.testequalidade.escola.business;

import java.sql.SQLException;
import java.time.LocalDate;

import br.ucsal.bes20221.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20221.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20221.testequalidade.escola.exception.EscolaNegocioException;
import br.ucsal.bes20221.testequalidade.escola.persistence.AlunoDAO;

public class AlunoBO {

	private static final int QTD_TAMANHO_MINIMO_NOME_ALUNO = 10;

	public static void incluir(Aluno aluno) throws SQLException, EscolaNegocioException {
		validar(aluno);
		AlunoDAO.salvar(aluno);
	}

	public static void atualizar(Aluno aluno) throws SQLException, EscolaNegocioException {
		validar(aluno);
		validarAtualizacao(aluno);
		AlunoDAO.salvar(aluno);
	}

	public static Integer calcularIdade(Integer matricula) throws SQLException {
		Aluno aluno = AlunoDAO.encontrarPorMatricula(matricula);
		System.out.println(LocalDate.now());
		return LocalDate.now().getYear() - aluno.getAnoNascimento();
	}

	private static void validarAtualizacao(Aluno aluno) throws EscolaNegocioException {
		if (SituacaoAluno.ATIVO.equals(aluno.getSituacao())) {
			throw new EscolaNegocioException("Não é possível atualizar alunos que não estejam ativos.");
		}
	}

	private static void validar(Aluno aluno) throws EscolaNegocioException {
		if (aluno.getNome() == null || aluno.getNome().trim().length() < QTD_TAMANHO_MINIMO_NOME_ALUNO) {
			throw new EscolaNegocioException("Nome de aluno muito curto.");
		}
		if (aluno.getAnoNascimento() == null || aluno.getAnoNascimento() > LocalDate.now().getYear()) {
			throw new EscolaNegocioException("Ano de nascimento de aluno inválido.");
		}
	}

}
