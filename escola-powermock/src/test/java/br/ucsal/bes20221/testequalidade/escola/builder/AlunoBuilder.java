package br.ucsal.bes20221.testequalidade.escola.builder;

import br.ucsal.bes20221.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20221.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

	private static final Integer MATRICULA_DEFAULT = 1;
	private static final String NOME_DEFAULT = "Claudio Neiva";
	private static final SituacaoAluno SITUACAO_DEFAULT = null;
	private static final Integer ANO_NASCIMENTO_DEFAULT = 1970;

	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoAluno situacao = SITUACAO_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;

	private AlunoBuilder() {
	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	// Costumamos enriquecer o builder com alguns configurações com semântica
	// aplicável nos nossos testes (como se fosse um ObjectMother).
	public static AlunoBuilder umAlunoAtivo() {
		return new AlunoBuilder().ativo();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder nascidoEm(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder comSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder ativo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder cancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	public AlunoBuilder mas() {
		return new AlunoBuilder().comMatricula(matricula).comNome(nome).comSituacao(situacao).nascidoEm(anoNascimento);
	}

	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
