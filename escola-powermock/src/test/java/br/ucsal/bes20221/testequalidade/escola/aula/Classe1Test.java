package br.ucsal.bes20221.testequalidade.escola.aula;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Classe1.class })
public class Classe1Test {

	// Neste método o foco está em mostrar que as chamadas originais ocorreram.
	public static void main(String[] args) {
		new Classe1().metodo4();
	}

	@Test
	public void testarMetodo4() throws Exception {
		Classe1 obj1 = new Classe1();
		Classe1 spy1 = PowerMockito.spy(obj1);
		
		PowerMockito.mockStatic(Classe1.class);
		PowerMockito.when(Classe1.metodo1()).thenReturn(10);
		PowerMockito.when(Classe1.class,"metodo2").thenReturn(20);
		
		// PowerMockito.when(spy1,"metodo3").thenReturn(30);
		PowerMockito.doReturn(30).when(spy1, "metodo3");
		
		spy1.metodo4();
	}

}
