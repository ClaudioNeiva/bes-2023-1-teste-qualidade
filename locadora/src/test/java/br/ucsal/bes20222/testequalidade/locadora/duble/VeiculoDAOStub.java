package br.ucsal.bes20222.testequalidade.locadora.duble;

import java.util.List;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

public class VeiculoDAOStub extends VeiculoDAO{

	private List<Veiculo> veiculos;

	public void configurarVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return veiculos;
	}

}
