package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

@TestInstance(Lifecycle.PER_CLASS)
class PersitenteLinkedListTest {

	private static final Long ID_LISTA = 1L;
	private static final String NOME_TABELA_LISTA = "lista1";
	private Connection connection;
	private PersistentList<String> lista;

	@BeforeAll
	void setupClass() throws SQLException {
		Assumptions.assumeTrue(DbUtil.isConnectionValid());
		connection = DbUtil.getConnection();
	}

	@BeforeEach
	void setup() throws SQLException {
		lista = new PersitenteLinkedList<>();
		lista.delete(ID_LISTA, connection, NOME_TABELA_LISTA);
	}

	@AfterAll
	void teardownClass() throws SQLException {
		Assumptions.assumeTrue(DbUtil.isConnectionValid());
		lista.delete(ID_LISTA, connection, NOME_TABELA_LISTA);
	}

	@Test
	void testarPersistirLista2Nomes()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {
		// Dados de entrada e saída esperada
		String nome0 = "claudio";
		String nome1 = "neiva";
		lista.add(nome0);
		lista.add(nome1);

		// Executando o método que está sendo testado (objetivo do teste)
		lista.persist(ID_LISTA, connection, NOME_TABELA_LISTA);

		// Recuperando o resultado atual
		PersitenteLinkedList<String> listaAtual = new PersitenteLinkedList<>();
		listaAtual.load(ID_LISTA, connection, NOME_TABELA_LISTA);

		// Comparar o resultado esperado com o resultado atual
		Assertions.assertAll(() -> Assertions.assertEquals(nome0, listaAtual.get(0)),
				() -> Assertions.assertEquals(nome1, listaAtual.get(1)));
	}

	@Test
	void testarPersistirLista5Nomes() throws SQLException {
		
	}
	
	@Test
	void testarLimpar() throws SQLException {

	}

	
}
